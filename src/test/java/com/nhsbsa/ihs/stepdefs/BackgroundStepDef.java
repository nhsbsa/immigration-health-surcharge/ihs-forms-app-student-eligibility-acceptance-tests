package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import com.nhsbsa.ihs.utilities.ConfigReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;

public class BackgroundStepDef {

    private WebDriver driver;
    private String baseUrl;
    private IHSPaidPage ihsPaidPage;
    private VisaQuestionPage visaQuestionPage;
    private EhicCardPage ehicCardPage;
    private HigherEducationPage higherEducationPage;
    private PaidWordPage paidWordPage;

    public BackgroundStepDef() {
        driver = Config.getDriver();
        baseUrl = ConfigReader.getEnvironment();
        ihsPaidPage = new IHSPaidPage(driver);
        visaQuestionPage = new VisaQuestionPage(driver);
        ehicCardPage = new EhicCardPage(driver);
        higherEducationPage = new HigherEducationPage(driver);
        paidWordPage = new PaidWordPage(driver);
    }

    @Given("^I launch the IHS Student eligibility application$")
    public void iLaunchTheIHSStudentEligibilityApplication() {
        driver.manage().deleteAllCookies();
        new Page(driver).navigateToUrl(baseUrl);
    }

    @And("^I have paid the immigration health surcharge$")
    public void iHavePaidTheImmigrationHealthSurcharge() {
        ihsPaidPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^I have the relevant visa$")
    public void iHaveTheRelevantVisa() {
        ihsPaidPage.selectYesRadioButtonAndClickContinue();
        visaQuestionPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^I am a valid EHIC card holder$")
    public void iAmAValidEHICCardHolder() {
        ihsPaidPage.selectYesRadioButtonAndClickContinue();
        visaQuestionPage.selectYesRadioButtonAndClickContinue();
        ehicCardPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^I am a student in higher education$")
    public void iAmAStudentInHigherEducation() {
        ihsPaidPage.selectYesRadioButtonAndClickContinue();
        visaQuestionPage.selectYesRadioButtonAndClickContinue();
        ehicCardPage.selectYesRadioButtonAndClickContinue();
        higherEducationPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^I am not carrying out any paid work in the UK$")
    public void iAmNotCarryingOutAnyPaidWorkInTheUK() {
        ihsPaidPage.selectYesRadioButtonAndClickContinue();
        visaQuestionPage.selectYesRadioButtonAndClickContinue();
        ehicCardPage.selectYesRadioButtonAndClickContinue();
        higherEducationPage.selectYesRadioButtonAndClickContinue();
        paidWordPage.selectNoRadioButtonAndClickContinue();
    }
}
