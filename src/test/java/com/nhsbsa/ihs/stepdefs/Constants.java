package com.nhsbsa.ihs.stepdefs;

public class Constants {

    public static final String START_PAGE_URL = "https://www.gov.uk/apply-student-immigration-health-surcharge-refund";
    public static final String START_PAGE_TITLE = "Get an immigration health surcharge refund if you're a student from the EU, Switzerland, Norway, Iceland or Liechtenstein - GOV.UK";

    public static final String NOT_ELIGIBLE_PAGE_URL = "/student-eligibility/applicant-not-eligible";
    public static final String IHS_PAID_PAGE_URL = "/student-eligibility/paid-immigration-health-surcharge";
    public static final String VISA_QUESTION_PAGE_URL = "/student-eligibility/have-visa-on-after-2021";
    public static final String EHIC_CARD_PAGE_URL = "/student-eligibility/have-european-health-insurance-card-ehic";
    public static final String FULL_TIME_STUDENT_PAGE_URL = "/student-eligibility/full-time-education";
    public static final String PAID_WORK_PAGE_URL = "/student-eligibility/paid-work-uk";
    public static final String LIVING_UK_PAGE_URL = "/student-eligibility/living-in-uk";
    public static final String CHECK_ANSWERS_PAGE_URL = "/student-eligibility/eligibility-check-your-answers";
    public static final String NAME_PAGE_URL = "/student-claim/name";
    public static final String COOKIES_POLICY_PAGE_URL = "/student-help/cookies";
    public static final String CONTACT_US_PAGE_URL = "/student-help/contact";
    public static final String ACCESSIBILITY_STATEMENT_PAGE_URL = "/student-help/accessibility-statement";
    public static final String TERMS_CONDITIONS_PAGE_URL = "/student-help/terms-conditions";
    public static final String PRIVACY_NOTICE_PAGE_URL = "/student-help/privacy-notice";

    public static final String NOT_ELIGIBLE_PAGE_TITLE = "The applicant is not eligible for an immigration health surcharge reimbursement - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String IHS_PAID_PAGE_TITLE = "Has the applicant paid an immigration health surcharge for their visa? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String VISA_QUESTION_PAGE_TITLE = "Does the applicant have a UK visa beginning on or after 1 January 2021? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String EHIC_CARD_PAGE_TITLE = "Does the applicant have a valid European Health Insurance Card (EHIC) issued from the EU, Switzerland, Norway, Iceland or Liechtenstein ? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String FULL_TIME_STUDENT_PAGE_TITLE = "Is the applicant a full-time student in higher education? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String PAID_WORK_PAGE_TITLE = "Is the applicant carrying out any paid work in the UK? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String LIVING_UK_PAGE_TITLE = "Is the applicant currently living in the UK? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String CHECK_ANSWERS_PAGE_TITLE = "Check your answers - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String NAME_PAGE_TITLE = "What's the applicant's name? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String COOKIES_POLICY_PAGE_TITLE = "Cookies - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String CONTACT_US_PAGE_TITLE = "Contact us - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String ACCESSIBILITY_STATEMENT_PAGE_TITLE = "Accessibility statement for Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String TERMS_CONDITIONS_PAGE_TITLE = "Terms and conditions - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String PRIVACY_NOTICE_PAGE_TITLE = "Privacy notice - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";

    public static final String EHIC_INFO_PAGE_TITLE = "European Health Insurance Card - European Commission";
    public static final String EHIC_INFO_PAGE_URL = "https://employment-social-affairs.ec.europa.eu/policies-and-activities/moving-working-europe/eu-social-security-coordination/european-health-insurance-card_en";
    public static final String GOV_UK_PAGE = "Welcome to GOV.UK";
    public static final String HELP_PAGE = "Help using GOV.UK - Help Pages - GOV.UK";
    public static final String OPEN_GOVERNMENT_LICENCE_PAGE = "Open Government Licence";
    public static final String CROWN_COPYRIGHT_PAGE = "Crown copyright - Re-using PSI";
    public static final String SNAP_SURVEY_PREVIEW_PAGE = "Survey Preview - Snap Surveys";
    public static final String STUDENT_SNAP_SURVEY_PAGE = "IHS Students Digital Feedback";
}