package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class CheckEligibilityAnswersStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private IHSPaidPage ihsPaidPage;
    private VisaQuestionPage visaQuestionPage;
    private EhicCardPage ehicCardPage;
    private HigherEducationPage higherEducationPage;
    private PaidWordPage paidWordPage;
    private LivingInUKPage livingInUKPage;
    private CheckEligibilityAnswersPage checkEligibilityAnswersPage;

    public CheckEligibilityAnswersStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        ihsPaidPage = new IHSPaidPage(driver);
        visaQuestionPage = new VisaQuestionPage(driver);
        ehicCardPage = new EhicCardPage(driver);
        higherEducationPage = new HigherEducationPage(driver);
        paidWordPage = new PaidWordPage(driver);
        livingInUKPage = new LivingInUKPage(driver);
        checkEligibilityAnswersPage = new CheckEligibilityAnswersPage(driver);
    }

    @When("^I am eligible for the immigration health surcharge reimbursement$")
    public void iAmEligibleForTheImmigrationHealthSurchargeReimbursement() {
        ihsPaidPage.selectYesRadioButtonAndClickContinue();
        visaQuestionPage.selectYesRadioButtonAndClickContinue();
        ehicCardPage.selectYesRadioButtonAndClickContinue();
        higherEducationPage.selectYesRadioButtonAndClickContinue();
        paidWordPage.selectNoRadioButtonAndClickContinue();
        livingInUKPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^I submit my eligibility answers$")
    public void iSubmitMyEligibilityAnswers() {
        checkEligibilityAnswersPage.checkAndAcceptButton();
    }

    @And("^I select (.*) on Check Eligibility Answers page$")
    public void iSelectHyperlinkOnCheckEligibilityAnswersPage(String hyperlink) {
        switch (hyperlink) {
            case "Service Name link":
                commonPage.serviceNameLink();
                break;
            case "GOV.UK link":
                commonPage.govUKLink();
                break;
            case "Back link":
                commonPage.backLink();
                break;
            case "Change IHS Paid link":
                checkEligibilityAnswersPage.changeLinkIHSPayment();
                break;
            case "Change Visa link":
                checkEligibilityAnswersPage.changeLinkVisaQuestion();
                break;
            case "Change EHIC link":
                checkEligibilityAnswersPage.changeLinkEHICCard();
                break;
            case "Change Education link":
                checkEligibilityAnswersPage.changeLinkHigherEducation();
                break;
            case "Change Paid Work link":
                checkEligibilityAnswersPage.changeLinkPaidWord();
                break;
            case "Change Residency link":
                checkEligibilityAnswersPage.changeLinkLivingUK();
                break;
        }
    }

    @Then("^I will see (.*) eligibility answers$")
    public void iWillSeeAnswersEligibilityAnswers(String answers) {
        Assert.assertEquals("Yes", checkEligibilityAnswersPage.getIHSPaidAnswer());
        Assert.assertEquals("Yes", checkEligibilityAnswersPage.getVisaAnswer());
        Assert.assertEquals("Yes", checkEligibilityAnswersPage.getEHICAnswer());
        Assert.assertEquals("Yes", checkEligibilityAnswersPage.getEducationAnswer());
        Assert.assertEquals("No", checkEligibilityAnswersPage.getPaidWordAnswer());
        Assert.assertEquals("Yes", checkEligibilityAnswersPage.getLivingInUKAnswer());
    }
}
