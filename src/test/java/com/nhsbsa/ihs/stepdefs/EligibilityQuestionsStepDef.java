package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

public class EligibilityQuestionsStepDef {

    private WebDriver driver;
    private IHSPaidPage ihsPaidPage;
    private VisaQuestionPage visaQuestionPage;
    private EhicCardPage ehicCardPage;
    private HigherEducationPage higherEducationPage;
    private PaidWordPage paidWordPage;
    private LivingInUKPage livingInUKPage;

    public EligibilityQuestionsStepDef() {
        driver = Config.getDriver();
        ihsPaidPage = new IHSPaidPage(driver);
        visaQuestionPage = new VisaQuestionPage(driver);
        ehicCardPage = new EhicCardPage(driver);
        higherEducationPage = new HigherEducationPage(driver);
        paidWordPage = new PaidWordPage(driver);
        livingInUKPage = new LivingInUKPage(driver);
    }

    @When("^I (.*) paid the Immigration Health Surcharge$")
    public void iOptionPaidTheImmigrationHealthSurcharge(String ihsOption) throws InterruptedException {
        switch (ihsOption) {
            case "have":
                ihsPaidPage.selectYesRadioButtonAndClickContinue();
                break;
            case "have not":
                ihsPaidPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                ihsPaidPage.continueButton();
                break;
        }
    }

    @When("^I (.*) have the relevant visa$")
    public void iVisaOptionHaveTheRelevantVisa(String visaOption) {
        switch (visaOption) {
            case "do":
                visaQuestionPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                visaQuestionPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                visaQuestionPage.continueButton();
                break;
        }
    }

    @When("^I (.*) have a valid EHIC card$")
    public void iEhicOptionHaveAValidEHICCard(String ehicOption) {
        switch (ehicOption) {
            case "do":
                ehicCardPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                ehicCardPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                ehicCardPage.continueButton();
                break;
        }
    }

    @When("^I (.*) full time student in higher education$")
    public void iStudentOptionFullTimeStudentInHigherEducation(String studentOption) {
        switch (studentOption) {
            case "am":
                higherEducationPage.selectYesRadioButtonAndClickContinue();
                break;
            case "am not":
                higherEducationPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                higherEducationPage.continueButton();
                break;
        }
    }

    @When("^I (.*) carrying out paid work in UK$")
    public void iWorkOptionCarryingOutPaidWorkInUK(String workOption) {
        switch (workOption) {
            case "am":
                paidWordPage.selectYesRadioButtonAndClickContinue();
                break;
            case "am not":
                paidWordPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                paidWordPage.continueButton();
                break;
        }
    }

    @When("^I (.*) currently residing in UK$")
    public void iResidingOptionCurrentlyResidingInUK(String residingOption) {
        switch (residingOption) {
            case "am":
                livingInUKPage.selectYesRadioButtonAndClickContinue();
                break;
            case "am not":
                livingInUKPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                livingInUKPage.continueButton();
                break;
        }
    }
}
