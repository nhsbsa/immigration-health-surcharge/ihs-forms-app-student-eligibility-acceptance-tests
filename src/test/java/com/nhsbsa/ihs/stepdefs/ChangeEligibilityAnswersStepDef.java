package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class ChangeEligibilityAnswersStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private IHSPaidPage ihsPaidPage;
    private VisaQuestionPage visaQuestionPage;
    private EhicCardPage ehicCardPage;
    private HigherEducationPage higherEducationPage;
    private PaidWordPage paidWorkPage;
    private LivingInUKPage livingInUKPage;
    private CheckEligibilityAnswersPage checkEligibilityAnswersPage;

    public ChangeEligibilityAnswersStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        ihsPaidPage = new IHSPaidPage(driver);
        visaQuestionPage = new VisaQuestionPage(driver);
        ehicCardPage = new EhicCardPage(driver);
        higherEducationPage = new HigherEducationPage(driver);
        paidWorkPage = new PaidWordPage(driver);
        livingInUKPage = new LivingInUKPage(driver);
        checkEligibilityAnswersPage = new CheckEligibilityAnswersPage(driver);
    }

    @And("^I select (.*) on the Check Eligibility Answers page$")
    public void iSelectChangelinkOnCheckEligibilityAnswersPage(String changelink) {
        switch (changelink) {
            case "Change IHS Paid link":
                checkEligibilityAnswersPage.changeLinkIHSPayment();
                break;
            case "Change Visa link":
                checkEligibilityAnswersPage.changeLinkVisaQuestion();
                break;
            case "Change EHIC link":
                checkEligibilityAnswersPage.changeLinkEHICCard();
                break;
            case "Change Education link":
                checkEligibilityAnswersPage.changeLinkHigherEducation();
                break;
            case "Change Paid Work link":
                checkEligibilityAnswersPage.changeLinkPaidWord();
                break;
            case "Change Residency link":
                checkEligibilityAnswersPage.changeLinkLivingUK();
                break;
        }
    }

    @Then("^I will see (.*) selected$")
    public void iWillSeeRadioButtonSelected(String radioButton) {
        switch (radioButton) {
            case "Paid IHS Yes":
                Assert.assertTrue(ihsPaidPage.isYesRadioButtonSelected());
                break;
            case "Visa Yes":
                Assert.assertTrue(visaQuestionPage.isYesRadioButtonSelected());
                break;
            case "EHIC Yes":
                Assert.assertTrue(ehicCardPage.isYesRadioButtonSelected());
                break;
            case "Higher Education Yes":
                Assert.assertTrue(higherEducationPage.isYesRadioButtonSelected());
                break;
            case "Paid Work No":
                Assert.assertTrue(paidWorkPage.isNoRadioButtonSelected());
                break;
            case "Living in UK Yes":
                Assert.assertTrue(livingInUKPage.isYesRadioButtonSelected());
                break;
    }
}

    @Then("I continue without changing my answer")
    public void iContinueWithoutChangingMyAnswer() {
        commonPage.clickContinueButton();
    }

    @Then("^I change my answer as (.*)$")
    public void iChangeMyAnswerAsRadioButton(String radioButton) {
        switch (radioButton) {
            case "Paid IHS No":
                ihsPaidPage.selectNoRadioButtonAndClickContinue();
                break;
            case "Visa No":
                visaQuestionPage.selectNoRadioButtonAndClickContinue();
                break;
            case "EHIC No":
                ehicCardPage.selectNoRadioButtonAndClickContinue();
                break;
            case "Higher Education No":
                higherEducationPage.selectNoRadioButtonAndClickContinue();
                break;
            case "Paid Work Yes":
                paidWorkPage.selectYesRadioButtonAndClickContinue();
                break;
            case "Living in UK No":
                livingInUKPage.selectNoRadioButtonAndClickContinue();
                break;
        }
    }

    @Then("^My answer remains the same as (.*)$")
    public void myAnswerRemainsTheSameAsAnswer(String answer) {
        switch (answer) {
            case "Paid IHS Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getIHSPaidAnswer().trim());
                break;
            case "Visa Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getVisaAnswer().trim());
                break;
            case "EHIC Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getEHICAnswer().trim());
                break;
            case "Higher Education Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getEducationAnswer().trim());
                break;
            case "Paid Work No":
                Assert.assertEquals("No", checkEligibilityAnswersPage.getPaidWordAnswer().trim());
                break;
            case "Living in UK Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getLivingInUKAnswer().trim());
                break;
        }
    }
}