package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;
import static org.hamcrest.CoreMatchers.is;

public class OutputValidationsStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private IHSPaidPage ihsPaidPage;
    private VisaQuestionPage visaQuestionPage;
    private EhicCardPage ehicCardPage;
    private HigherEducationPage higherEducationPage;
    private PaidWordPage paidWordPage;
    private LivingInUKPage livingInUKPage;
    private Page page;
    private NotEligiblePage notEligiblePage;

    public OutputValidationsStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        ihsPaidPage = new IHSPaidPage(driver);
        visaQuestionPage = new VisaQuestionPage(driver);
        ehicCardPage = new EhicCardPage(driver);
        higherEducationPage = new HigherEducationPage(driver);
        paidWordPage = new PaidWordPage(driver);
        livingInUKPage = new LivingInUKPage(driver);
        page = new Page(driver);
        notEligiblePage = new NotEligiblePage(driver);
    }

    @Then("^I will see the (.*)$")
    public void iWillSeeTheOutput(String output) {
        switch (output) {
            case "Start screen":
                Assert.assertEquals(START_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(START_PAGE_URL));
                break;
            case "Not Eligible screen":
                Assert.assertEquals(NOT_ELIGIBLE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NOT_ELIGIBLE_PAGE_URL));
                break;
            case "IHS Paid screen":
                Assert.assertEquals(IHS_PAID_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(IHS_PAID_PAGE_URL));
                break;
            case "Visa Question screen":
                Assert.assertEquals(VISA_QUESTION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(VISA_QUESTION_PAGE_URL));
                break;
            case "EHIC Card screen":
                Assert.assertEquals(EHIC_CARD_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(EHIC_CARD_PAGE_URL));
                break;
            case "Higher Education screen":
                Assert.assertEquals(FULL_TIME_STUDENT_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(FULL_TIME_STUDENT_PAGE_URL));
                break;
            case "Paid Work screen":
                Assert.assertEquals(PAID_WORK_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(PAID_WORK_PAGE_URL));
                break;
            case "Living in UK screen":
                Assert.assertEquals(LIVING_UK_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(LIVING_UK_PAGE_URL));
                break;
            case "Check Eligibility Answers screen":
                Assert.assertEquals(CHECK_ANSWERS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_ANSWERS_PAGE_URL));
                break;
            case "Name screen":
                Assert.assertEquals(NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NAME_PAGE_URL));
                break;
            case "GOV UK screen":
                Assert.assertEquals(GOV_UK_PAGE, commonPage.getCurrentPageTitle());
                break;
            case "EHIC Info screen":
                Assert.assertEquals(EHIC_INFO_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                ehicCardPage.navigateToEHICInfo();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(EHIC_INFO_PAGE_URL));
                break;
            case "Help screen":
                Assert.assertEquals(HELP_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Open Government Licence screen":
                Assert.assertEquals(OPEN_GOVERNMENT_LICENCE_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Crown Copyright screen":
                Assert.assertEquals(CROWN_COPYRIGHT_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Service Start screen":
                Assert.assertEquals(START_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                notEligiblePage.navigateToMoreInfo();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(START_PAGE_URL));
                break;
            case "Snap survey preview":
                Assert.assertEquals(SNAP_SURVEY_PREVIEW_PAGE, commonPage.getSurveyPreviewPageTitle());
                break;
            case "IHS Students Service Survey screen":
                Assert.assertEquals(STUDENT_SNAP_SURVEY_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Contact Us screen":
                Assert.assertEquals(CONTACT_US_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToContactUs();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(CONTACT_US_PAGE_URL));
                break;
            case "Cookies Policy screen":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToCookies();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(COOKIES_POLICY_PAGE_URL));
                break;
            case "Accessibility Statement screen":
                Assert.assertEquals(ACCESSIBILITY_STATEMENT_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToAccessibilityStatement();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(ACCESSIBILITY_STATEMENT_PAGE_URL));
                break;
            case "Terms and Conditions screen":
                Assert.assertEquals(TERMS_CONDITIONS_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToTermsConditions();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(TERMS_CONDITIONS_PAGE_URL));
                break;
            case "Privacy Notice screen":
                Assert.assertEquals(PRIVACY_NOTICE_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToPrivacyNotice();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(PRIVACY_NOTICE_PAGE_URL));
                break;
            case "Cookies Policy page":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "IHS Payment Error Message":
                Assert.assertEquals("Select 'Yes' if the applicant paid an immigration health surcharge (IHS) for their visa", ihsPaidPage.getErrorMessage());
                break;
            case "Visa Question Error Message":
                Assert.assertEquals("Select 'Yes' if the applicant has a UK visa beginning on or after 1 January 2021", visaQuestionPage.getErrorMessage());
                break;
            case "EHIC Question Error Message":
                Assert.assertEquals("Select 'Yes' if the applicant has a valid European Health Insurance Card (EHIC) issued from the EU, Switzerland, Norway, Iceland or Liechtenstein", ehicCardPage.getErrorMessage());
                break;
            case "Higher Education Error Message":
                Assert.assertEquals("Select 'Yes' if the applicant is a full-time student in higher education", higherEducationPage.getErrorMessage());
                break;
            case "Paid Work Error Message":
                Assert.assertEquals("Select 'Yes' if the applicant is carrying out any paid work in the UK", paidWordPage.getErrorMessage());
                break;
            case "Living in UK Error Message":
                Assert.assertEquals("Select 'Yes' if the applicant is currently living in the UK", livingInUKPage.getErrorMessage());
                break;
            case "Claim Name screen":
                try {
                    page.waitForPageLoad();
                    Assert.assertTrue(commonPage.getCurrentURL().contains(NAME_PAGE_URL));
                    ((JavascriptExecutor) driver).executeScript("sauce:job-result=passed");
                } catch (AssertionError e) {
                    Assert.assertThat(e.getMessage(),is("My assertion error"));
                    ((JavascriptExecutor) driver).executeScript("sauce:job-result=failed");
                }
                break;
        }
    }

    @And("^My Page Title is (.*) title$")
    public void myPageTitleIsScreenTitle(String screen) {
        switch (screen) {
            case "IHS Paid screen":
                Assert.assertEquals(IHS_PAID_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Visa Question screen":
                Assert.assertEquals(VISA_QUESTION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "EHIC Card screen":
                Assert.assertEquals(EHIC_CARD_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Higher Education screen":
                Assert.assertEquals(FULL_TIME_STUDENT_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Paid Work screen":
                Assert.assertEquals(PAID_WORK_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Living in UK screen":
                Assert.assertEquals(LIVING_UK_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Check Eligibility Answers screen":
                Assert.assertEquals(CHECK_ANSWERS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Name screen":
                Assert.assertEquals(NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Not Eligible screen":
                Assert.assertEquals(NOT_ELIGIBLE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Contact Us screen":
                Assert.assertEquals(CONTACT_US_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Cookies Policy screen":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Accessibility Statement screen":
                Assert.assertEquals(ACCESSIBILITY_STATEMENT_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Terms and Conditions screen":
                Assert.assertEquals(TERMS_CONDITIONS_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Privacy Notice screen":
                Assert.assertEquals(PRIVACY_NOTICE_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
        }
    }
}
