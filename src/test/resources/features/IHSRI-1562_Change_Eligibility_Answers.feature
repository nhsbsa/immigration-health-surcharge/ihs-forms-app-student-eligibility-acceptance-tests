@ChangeEligibilityAnswers @IHSRI-1562 @Regression

Feature: Validation of Change Links on Check Your Answers page on Eligibility form app to verify the applicant's eligibility for IHS reimbursement

  Background:
    Given I launch the IHS Student eligibility application
    And I am eligible for the immigration health surcharge reimbursement

  Scenario Outline: Validate the change links on Check Eligibility Answers page without changing the answer
    When I select <changeLink> on the Check Eligibility Answers page
    Then I will see the <screen>
    And I will see <radioButton> selected
    When I continue without changing my answer
    Then I will see the Check Eligibility Answers screen
    And My answer remains the same as <answer>

    Examples:
      | changeLink            | screen                  | radioButton          | answer               |
      | Change IHS Paid link  | IHS Paid screen         | Paid IHS Yes         | Paid IHS Yes         |
      | Change Visa link      | Visa Question screen    | Visa Yes             | Visa Yes             |
      | Change EHIC link      | EHIC Card screen        | EHIC Yes             | EHIC Yes             |
      | Change Education link | Higher Education screen | Higher Education Yes | Higher Education Yes |
      | Change Paid Work link | Paid Work screen        | Paid Work No         | Paid Work No         |
      | Change Residency link | Living in UK screen     | Living in UK Yes     | Living in UK Yes     |

  Scenario Outline: Validate the change links on Check Eligibility Answers page on changing the answer
    When I select <changeLink> on Check Eligibility Answers page
    Then I will see the <screen>
    When I change my answer as <radioButton>
    Then I will see the Not Eligible screen
    Examples:
      | changeLink            | screen                  | radioButton          |
      | Change IHS Paid link  | IHS Paid screen         | Paid IHS No          |
      | Change Visa link      | Visa Question screen    | Visa No              |
      | Change EHIC link      | EHIC Card screen        | EHIC No              |
      | Change Education link | Higher Education screen | Higher Education No  |
      | Change Paid Work link | Paid Work screen        | Paid Work Yes        |
      | Change Residency link | Living in UK screen     | Living in UK No      |

  Scenario Outline: Validate the navigation to the Check Eligibility Answers Page from change links
    When I select <changeLink> on Check Eligibility Answers page
    Then I will see the <screen>
    When I select the Back link
    And I continue without changing my answer
    Then I will see the Check Eligibility Answers screen
    Examples:
      | changeLink            | screen                  |
      | Change Visa link      | Visa Question screen    |
      | Change EHIC link      | EHIC Card screen        |
      | Change Education link | Higher Education screen |
      | Change Paid Work link | Paid Work screen        |
      | Change Residency link | Living in UK screen     |