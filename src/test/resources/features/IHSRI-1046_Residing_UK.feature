@ResidingUK @IHSRI-1046 @Regression

Feature: Validation of Living in UK page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Student eligibility application
    And I am not carrying out any paid work in the UK

  @Smoke
  Scenario Outline: Validate the various options an applicant can select on Living in UK page
    When I <residingOption> currently residing in UK
    Then I will see the <output>
    Examples:
      | residingOption | output                           |
      | am             | Check Eligibility Answers screen |
      | am not         | Not Eligible screen              |
      |                | Living in UK Error Message       |

  Scenario Outline: Validate the hyperlinks on Living in UK page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output           |
      | Service Name link | Start screen     |
      | Back link         | Paid Work screen |
      | GOV.UK link       | GOV UK screen    |

