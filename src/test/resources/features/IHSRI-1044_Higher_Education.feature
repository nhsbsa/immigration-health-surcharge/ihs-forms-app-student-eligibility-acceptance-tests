@HigherEducation @IHSRI-1044 @Regression

Feature: Validation of Higher Education page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Student eligibility application
    And I am a valid EHIC card holder

  @Smoke
  Scenario Outline: Validate the various options an applicant can select on Higher Education page
    When I <studentOption> full time student in higher education
    Then I will see the <output>
    Examples:
      | studentOption | output                         |
      | am            | Paid Work screen               |
      | am not        | Not Eligible screen            |
      |               | Higher Education Error Message |

  Scenario Outline: Validate the hyperlinks on Higher Education page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output           |
      | Service Name link | Start screen     |
      | Back link         | EHIC Card screen |
      | GOV.UK link       | GOV UK screen    |

