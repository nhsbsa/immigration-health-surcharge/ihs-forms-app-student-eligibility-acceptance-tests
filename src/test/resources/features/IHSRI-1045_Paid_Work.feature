@PaidWork @IHSRI-1045 @Regression

Feature: Validation of Paid Work page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Student eligibility application
    And I am a student in higher education

  @Smoke
  Scenario Outline: Validate the various options an applicant can select on Paid Work page
    When I <workOption> carrying out paid work in UK
    Then I will see the <output>
    Examples:
      | workOption | output                  |
      | am         | Not Eligible screen     |
      | am not     | Living in UK screen     |
      |            | Paid Work Error Message |

  Scenario Outline: Validate the hyperlinks on Paid Work page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                  |
      | Service Name link | Start screen            |
      | Back link         | Higher Education screen |
      | GOV.UK link       | GOV UK screen           |

