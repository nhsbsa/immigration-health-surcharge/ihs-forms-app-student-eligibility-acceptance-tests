@Accessibility

Feature: Validation of the accessibility of IHS Student eligibility app using Axe, Lighthouse and Wave tools

  Scenario Outline: Validate the end-to-end accessibility of IHS Student eligibility app
    Given I launch the IHS Student eligibility application on <browser>
    When I have paid the Immigration Health Surcharge
    And I do have the relevant visa
    And I do have a valid EHIC card
    And I am full time student in higher education
    And I am not carrying out paid work in UK
    And I am currently residing in UK
    And I submit my eligibility answers
    Examples:
      | browser |
      | chrome  |
      | firefox |

 Scenario Outline: Validate the accessibility for Not Eligible screen
    Given I launch the IHS Student eligibility application on <browser>
    When I have not paid the Immigration Health Surcharge
    Examples:
    | browser |
    | chrome  |
    | firefox |