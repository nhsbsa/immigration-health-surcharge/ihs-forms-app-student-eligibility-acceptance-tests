@VisaQuestion @IHSRI-1042 @Regression

Feature: Validation of Visa Question page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Student eligibility application
    And I have paid the immigration health surcharge

  @Smoke @IHSRI-1723
  Scenario Outline: Validate the various options an applicant can select on Visa Question page
    When I <visaOption> have the relevant visa
    Then I will see the <output>
    Examples:
      | visaOption | output                      |
      | do         | EHIC Card screen            |
      | do not     | Not Eligible screen         |
      |            | Visa Question Error Message |

  Scenario Outline: Validate the hyperlinks on Visa Question page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output          |
      | Service Name link | Start screen    |
      | Back link         | IHS Paid screen |
      | GOV.UK link       | GOV UK screen   |

