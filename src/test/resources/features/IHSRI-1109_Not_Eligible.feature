@NotEligible @IHSRI-1109 @Regression

Feature: Validation of Not Eligible page when applicant is not eligible for IHS reimbursement.

  Background:
    Given I launch the IHS Student eligibility application

  @Smoke @IHSRI-2816
  Scenario Outline: Validate the hyperlinks on Not Eligible for Reimbursement page
    When I <ihsOption> paid the Immigration Health Surcharge
    And I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | ihsOption | hyperlink         | output               |
      | have not  | Service Name link | Start screen         |
      | have not  | GOV.UK link       | GOV UK screen        |
      | have not  | Contact Us link   | Contact Us screen    |
      | have not  | More Info link    | Service Start screen |