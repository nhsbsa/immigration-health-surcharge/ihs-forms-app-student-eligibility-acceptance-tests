@IHSStudents @ServiceStartPage @ManualTest

Feature: Validation of Gov.UK IHS Service Start page to verify the navigation of links for Immigration Health Surcharge Reimbursement as a Student

  #  This test needs to be performed manually
  Scenario Outline: Validate the various links an Gov.UK IHS Service start page for Immigration Health Surcharge Reimbursement
    Given I launch the Gov.UK start page
    When I click on <links>
    Then I will see <output> page
    Examples:
      | links                   | output                                                                                                                         |
      # If you’re a student from Norway
      | contact the NHSBSA      | https://get-an-immigration-health-surcharge-reimbursement.service.gov.uk/student-help/contact?hof-cookie-check                 |
      # If you’re a student from Denmark
      | contact the NHSBSA      | https://get-an-immigration-health-surcharge-reimbursement.service.gov.uk/student-help/contact                                  |
      # Below start button
      | contact the NHSBSA      | https://get-an-immigration-health-surcharge-reimbursement.service.gov.uk/student-help/contact                                  |
      # Start Button
      | Start now               | https://get-an-immigration-health-surcharge-reimbursement.service.gov.uk/student-eligibility/paid-immigration-health-surcharge |
