@FooterLinks @IHSRI-1476 @Regression

Feature: Validation of footer links on IHS Student reimbursement service

  Background:
    Given I launch the IHS Student eligibility application

  @Smoke
  Scenario Outline: Validate the footer links navigation on IHS student service
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink                    | output                         |
      | Help link                    | Help screen                    |
      | Cookies link                 | Cookies Policy screen          |
      | Accessibility Statement link | Accessibility Statement screen |
      | Contact link                 | Contact Us screen              |
      | Terms and Conditions link    | Terms and Conditions screen    |
      | Privacy link                 | Privacy Notice screen          |
      | Open Government Licence link | Open Government Licence screen |
      | Crown Copyright link         | Crown Copyright screen         |