@IHSPaid @IHSRI-1041 @Regression

Feature: Validation of IHS Paid page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Student eligibility application

  @Smoke
  Scenario Outline: Validate the various options an applicant can select on IHS Payment Question page
    When I <ihsOption> paid the Immigration Health Surcharge
    Then I will see the <output>
    Examples:
      | ihsOption | output                    |
      | have      | Visa Question screen      |
      | have not  | Not Eligible screen       |
      |           | IHS Payment Error Message |

  @IHSRI-3051
  Scenario Outline: Validate the hyperlinks on IHS Payment page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output        |
      | Service Name link | Start screen  |
      | GOV.UK link       | GOV UK screen |

