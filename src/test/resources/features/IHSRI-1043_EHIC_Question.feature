@EHICQuestion @IHSRI-1043 @Regression

Feature: Validation of EHIC Question page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Student eligibility application
    And I have the relevant visa

  @Smoke @IHSRI-1724 @IHSRI-3053
  Scenario Outline: Validate the various options an applicant can select on EHIC Question page
    When I <ehicOption> have a valid EHIC card
    Then I will see the <output>
    Examples:
      | ehicOption | output                      |
      | do         | Higher Education screen     |
      | do not     | Not Eligible screen         |
      |            | EHIC Question Error Message |

  @IHSRI-2816
  Scenario Outline: Validate the hyperlinks on EHIC Question page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output               |
      | Service Name link | Start screen         |
      | Back link         | Visa Question screen |
      | GOV.UK link       | GOV UK screen        |
      | EHIC info link    | EHIC Info screen     |

