Feature: Validation of the IHS Student Eligibility end-to-end functionality in various SauceLabs devices to meet the compatibility test criteria

  @Compatibility
  Scenario Outline: Validate the applicant is navigated to claim application after submitting the eligibility answers
    Given I launch the IHS Student eligibility application
    When I <ihsOption> paid the Immigration Health Surcharge
    And I <visaOption> have the relevant visa
    And I <ehicOption> have a valid EHIC card
    And I <studentOption> full time student in higher education
    And I <workOption> carrying out paid work in UK
    And I <residingOption> currently residing in UK
    And I submit my eligibility answers
    Then I will see the <output>

    Examples:
      | ihsOption | visaOption | ehicOption | studentOption | workOption | residingOption | output            |
      | have      | do         | do         | am            | am not     | am             | Claim Name screen |