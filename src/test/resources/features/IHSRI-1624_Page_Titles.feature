@PageTitles @IHSRI-1624 @Regression

Feature: Validation of page title on the IHS student eligibility application screens

  Background:
    Given I launch the IHS Student eligibility application

  @IHSRI-3053
  Scenario Outline: Verify the page title displayed on student eligibility application pages
    And My Page Title is IHS Paid screen title
    And I <ihsOption> paid the Immigration Health Surcharge
    And My Page Title is Visa Question screen title
    And I <visaOption> have the relevant visa
    And My Page Title is EHIC Card screen title
    And I <ehicOption> have a valid EHIC card
    And My Page Title is Higher Education screen title
    And I <studentOption> full time student in higher education
    And My Page Title is Paid Work screen title
    And I <workOption> carrying out paid work in UK
    And My Page Title is Living in UK screen title
    And I <residingOption> currently residing in UK
    And My Page Title is Check Eligibility Answers screen title
    When I submit my eligibility answers
    Then I will see the <output>
    And My Page Title is Name screen title

    Examples:
      | ihsOption | visaOption | ehicOption | studentOption | workOption | residingOption | output      |
      | have      | do         | do         | am            | am not     | am             | Name screen |

  Scenario Outline: Verify the page title displayed on Not Eligible page
    When I <ihsOption> paid the Immigration Health Surcharge
    Then I will see the <screen>
    And My Page Title is <screen> title

    Examples:
      | ihsOption | screen |
      | have not  | Not Eligible screen |

  Scenario Outline: Verify the page title displayed on footer link pages
    When I select the <hyperlink>
    Then My Page Title is <screen> title

    Examples:
      | hyperlink                    | screen                         |
      | Contact link                 | Contact Us screen              |
      | Cookies link                 | Cookies Policy screen          |
      | Accessibility Statement link | Accessibility Statement screen |
      | Terms and Conditions link    | Terms and Conditions screen    |
      | Privacy link                 | Privacy Notice screen          |