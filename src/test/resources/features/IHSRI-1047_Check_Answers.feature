@CheckEligibilityAnswers @IHSRI-1047 @Regression

Feature: Validation of Check Your Answers page on Eligibility form app to verify the applicant's eligibility for IHS reimbursement

  Background:
    Given I launch the IHS Student eligibility application

  @Smoke
  Scenario Outline: Validate the hyperlinks on Check Eligibility Answers page
    When I am eligible for the immigration health surcharge reimbursement
    And I select <hyperlink> on Check Eligibility Answers page
    Then I will see the <output>

    Examples:
      | hyperlink             | output                  |
      | Back link             | Living in UK screen     |
      | Service Name link     | Start screen            |
      | GOV.UK link           | GOV UK screen           |

  Scenario Outline: Validate the applicant is able to verify the submitted eligibility answers
    When I <ihsOption> paid the Immigration Health Surcharge
    And I <visaOption> have the relevant visa
    And I <ehicOption> have a valid EHIC card
    And I <studentOption> full time student in higher education
    And I <workOption> carrying out paid work in UK
    And I <residingOption> currently residing in UK
    Then I will see <answers> eligibility answers

    Examples:
      | ihsOption | visaOption | ehicOption | studentOption | workOption | residingOption | answers                                |
      | have      | do         | do         | am            | am not     | am             | 1=Yes 2=Yes 3=Yes 4=Yes 5=No and 6=Yes |

  @Smoke @EndToEnd
  Scenario Outline: Validate the applicant is navigated to claim application after submitting the eligibility answers
    When I <ihsOption> paid the Immigration Health Surcharge
    And I <visaOption> have the relevant visa
    And I <ehicOption> have a valid EHIC card
    And I <studentOption> full time student in higher education
    And I <workOption> carrying out paid work in UK
    And I <residingOption> currently residing in UK
    And I submit my eligibility answers
    Then I will see the <output>

    Examples:
      | ihsOption | visaOption | ehicOption | studentOption | workOption | residingOption | output      |
      | have      | do         | do         | am            | am not     | am             | Name screen |