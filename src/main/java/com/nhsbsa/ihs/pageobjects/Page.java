package com.nhsbsa.ihs.pageobjects;

import io.cucumber.java.Scenario;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Page {

    protected WebDriver driver;
    private WebElement element;
    public int explicitWaitTime = 50;
    public WebDriverWait webDriverWait;
    Logger logger = Logger.getLogger(Page.class.getName());

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToUrl(String url) {
        driver.get(url);
    }

    public WebDriverWait getWebDriverWait() {
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(explicitWaitTime));
        webDriverWait.ignoring(NoSuchElementException.class);
        webDriverWait.ignoring(StaleElementReferenceException.class);
        return webDriverWait;
    }

    public void waitForPageLoad() {
        Wait<WebDriver> driverWait = new WebDriverWait(driver, Duration.ofSeconds(explicitWaitTime));
        driverWait.until(d -> {
            logger.log(Level.INFO,"Current Window State: {0}",String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
            return String
                    .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                    .equals("complete");
        });
    }

    public void clickEvent(By by) {
        webDriverWait = getWebDriverWait();
        WebElement ele = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(by));
        ele.click();
    }

    public void sendTextValues(By by, String text) {
        webDriverWait = getWebDriverWait();
        WebElement ele = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(by));
        ele.clear();
        ele.sendKeys(text);
    }

    public void checkScenarioRunStatus(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver)
                    .getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot,"image/jpeg", "failed screen" );
        }
    }

    public boolean getElementIsDisplayed(By by) {
        return driver.findElement(by).isDisplayed();
    }

    public String getElementText() {
        return element.getText();
    }

    public String getElementText(By by) {
        return driver.findElement(by).getText();
    }

    public String getPageTitles(){
        return driver.getTitle();
    }

    public void click() {
        element.click();
    }

    public void tearDownDriver() {
        driver.quit();
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public void goBack() {
        driver.navigate().back();
    }

    public void navigateToElementBy(By by) {
        element = element.findElement(by);
    }

    public String getAttributeValue(String val) {
        return element.getAttribute(val);
    }

    public String getElementValue() {
        return element.getAttribute("value");
    }

    public void clickEventJS(By by){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement ele = driver.findElement(by);
        js.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 500);");
        ele.click();
    }

    public void sendTextValuesJS(By by, String text) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement ele = driver.findElement(by);
        js.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 500);");
        ele.clear();
        ele.sendKeys(text);
    }
}
