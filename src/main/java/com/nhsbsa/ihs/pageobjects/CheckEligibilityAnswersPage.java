package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckEligibilityAnswersPage extends Page {

    private By checkAndAcceptButtonLocator = By.id("student-claim-app-link");
    private By changeLinkIHSPaymentLocator = By.id("/paid-immigration-health-surcharge-change-link");
    private By changeLinkVisaQuestionLocator = By.id("/have-visa-on-after-2021-change-link");
    private By changeLinkEhicCardLocator = By.id("/have-european-health-insurance-card-ehic-change-link");
    private By changeLinkHigherEducationLocator = By.id("/full-time-education-change-link");
    private By changeLinkPaidWordLocator = By.id("/paid-work-uk-change-link");
    private By changeLinkLivingUKLocator = By.id("/living-in-uk-change-link");
    private By ihsAnswer = By.id("/paid-immigration-health-surcharge-value");
    private By visaAnswer = By.id("/have-visa-on-after-2021-value");
    private By ehicAnswer = By.id("/have-european-health-insurance-card-ehic-value");
    private By educationAnswer = By.id("/full-time-education-value");
    private By workAnswer = By.id("/paid-work-uk-value");
    private By residencyAnswer = By.id("/living-in-uk-value");

    public CheckEligibilityAnswersPage(WebDriver driver) {
        super(driver);
    }

    public void checkAndAcceptButton() {
        Page page = new Page(driver);
        page.waitForPageLoad();
        clickEvent(checkAndAcceptButtonLocator);
    }

    public void changeLinkIHSPayment() {
        clickEvent(changeLinkIHSPaymentLocator);
    }
    public void changeLinkVisaQuestion() {
        clickEvent(changeLinkVisaQuestionLocator);
    }
    public void changeLinkEHICCard() {
        clickEvent(changeLinkEhicCardLocator);
    }
    public void changeLinkHigherEducation() {
        clickEvent(changeLinkHigherEducationLocator);
    }
    public void changeLinkPaidWord() {
        clickEvent(changeLinkPaidWordLocator);
    }
    public void changeLinkLivingUK() {
        clickEvent(changeLinkLivingUKLocator);
    }

    public String getIHSPaidAnswer() {
        return getElementText(ihsAnswer);
    }

    public String getVisaAnswer() {
        return getElementText(visaAnswer);
    }

    public String getEHICAnswer() {
        return getElementText(ehicAnswer);
    }

    public String getEducationAnswer() {
        return getElementText(educationAnswer);
    }

    public String getPaidWordAnswer() {
        return getElementText(workAnswer);
    }

    public String getLivingInUKAnswer() {
        return getElementText(residencyAnswer);
    }

}
