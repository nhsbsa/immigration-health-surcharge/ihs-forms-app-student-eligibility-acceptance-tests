package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class EhicCardPage extends Page {

    private By yesRadioButtonLocator = By.id("have-european-health-insurance-card-ehic-yes");
    private By noRadioButtonLocator = By.id("have-european-health-insurance-card-ehic-no");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select 'Yes' if");
    private By ehicInfoLocator = By.id("ehic-more-info-link");

    public EhicCardPage(WebDriver driver) {
        super(driver);
    }

    public void selectYesRadioButtonAndClickContinue() {
        clickEvent(yesRadioButtonLocator);
        continueButton();
    }

    public void selectNoRadioButtonAndClickContinue() {
        clickEvent(noRadioButtonLocator);
        continueButton();
    }

    public void continueButton() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", driver.findElement(continueButtonLocator));
        clickEvent(continueButtonLocator);
    }

    public void navigateToEHICInfo() {
        clickEvent(ehicInfoLocator);
    }

    public String getErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public boolean isYesRadioButtonSelected() {
        return driver.findElement(yesRadioButtonLocator).isSelected();
    }
}