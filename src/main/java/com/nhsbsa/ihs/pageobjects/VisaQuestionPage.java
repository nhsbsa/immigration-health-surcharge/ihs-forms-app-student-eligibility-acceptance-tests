package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class VisaQuestionPage extends Page {

    private By yesRadioButtonLocator = By.id("have-visa-on-after-2021-yes");
    private By noRadioButtonLocator = By.id("have-visa-on-after-2021-no");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select 'Yes' if");

    public VisaQuestionPage(WebDriver driver) {
        super(driver);
    }

    public void selectYesRadioButtonAndClickContinue() {
        clickEvent(yesRadioButtonLocator);
        continueButton();
    }

    public void selectNoRadioButtonAndClickContinue() {
        clickEvent(noRadioButtonLocator);
        continueButton();
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String getErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public boolean isYesRadioButtonSelected() {
        return driver.findElement(yesRadioButtonLocator).isSelected();
    }
}
